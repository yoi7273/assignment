import React, { Component } from 'react'
import axios from 'axios';
export default class Table extends Component {
    constructor(props) {
        super(props);
        this.state = {
            vintageData: [],
            TotalPrice: "",
            TotalQunatity: ""
        }
    }
    componentDidMount() {
        this.getData();
    }
    getData = () => {
        let url = "http://localhost:8080/getVintage";
        axios.get(url).then(
            (response) => {
                let totalprice = 0;
                let totalqunatity = 0;
                for (let i = 0; i < response.data.length; i++) {
                    let element = response.data[i];
                    element.Total = element.quntity * element.price;
                    totalprice = totalprice + (element.quntity * element.price);
                    totalqunatity = totalqunatity + Number(element.quntity);
                }
                this.setState({ vintageData: response.data });
                this.setState({ TotalPrice: totalprice });
                this.setState({ TotalQunatity: totalqunatity })
            }
        )
    }
    deleteData = (id) => {
        let url = "http://localhost:8080/deleteVintage";
        axios.post(url, { 'id': id }).then((res) => {
            window.location.reload();
        })
    }
    render() {
        const table = this.state.vintageData
        const totalPrice = this.state.TotalPrice
        const totalQunatity = this.state.TotalQunatity
        return (
            <>
                <div className="container text-center">
                    <div className="row justify-content-md-center">
                        <table className="table table-stripped">
                            <thead>
                                <tr>
                                    <th>Year</th>
                                    <th>Qunatity</th>
                                    <th>Price per Unit</th>
                                    <th>Total Price</th>
                                </tr>
                                <tr></tr>
                            </thead>
                            <tbody>
                                {table.map((obj) => (
                                    <tr>
                                        <th>{obj.year}</th>
                                        <th>{obj.quntity}</th>
                                        <th>{obj.price}</th>
                                        <th>{obj.Total}</th>
                                        <th>
                                            <svg onClick={() => this.deleteData(obj.id)} values={obj.id} xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash" viewBox="0 0 16 16" style={{cursor:'pointer'}}>
                                                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                                <path fillRule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                                            </svg>
                                        </th>
                                    </tr>
                                ))}
                                <tr>
                                    <th>
                                        Total
                                    </th>
                                    <th>
                                        {totalQunatity} metric ton(s)
                                    </th>
                                    <th></th>
                                    <th>
                                        {totalPrice} dollar(s)
                                    </th>
                                    <th>
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </>
        )
    }
}
