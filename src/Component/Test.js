import React, { useState } from "react";
import { YearRangePicker } from "react-year-range-picker";
import axios from 'axios';
export default function Test() {
    const [yearRange, setYearRange] = useState();
    const [cQunatity, setCQunatity] = useState(0);
    const [priceUnit, setPriceUnit] = useState(0);
    const changeQunatity = (event) => { setCQunatity(event.target.value); };
    const changePrice = (event) => {
        setPriceUnit(event.target.value);
    };
    const postData = (e) => {
        e.preventDefault();
        localStorage.setItem("startyear", yearRange.startYear);
        localStorage.setItem("endyear", yearRange.endYear);
        const start = localStorage.getItem("startyear");
        const end = localStorage.getItem("endyear");
        let result = start.concat(-end);
        let url = "http://localhost:8080/addVintage";
        axios.post(url, {
            year: result,
            quntity: cQunatity,
            price: priceUnit
        }).then((res) => {
            console.log(res.data);
            window.location.reload();
        });
    }
    return (
        <>
            <form>
                <div className="container text-center">
                    <div className="row justify-content-md-center">
                        <div className="col col-lg-2">
                            <label>Vintage Year</label>
                            <YearRangePicker
                                minYear={new Date().getFullYear() - 2}
                                maxYear={new Date().getFullYear() + 2}
                                onSelect={(startYear, endYear) => {
                                    setYearRange({ startYear, endYear });
                                }}
                                startYear={yearRange?.startYear}
                                endYear={yearRange?.endYear}
                            />
                        </div>
                        <div className="col col-lg-2">
                            <label>Credit Qunatity</label>
                            <input type='number' min='0' value={cQunatity} onChange={changeQunatity} id="creditQunatity" />
                            <label>metric ton(s)</label>
                        </div>
                        <div className="col col-lg-2">
                            <label>Price per Unit</label>
                            <input type='number' min='0' value={priceUnit} onChange={changePrice} id="priceperUnit" />
                            <label>dollar(s)</label>
                        </div>
                        <div className="col col-lg-2">
                            {/* <a href="#" className="link-success">+Add Vintage</a> */}
                            <button type="button" onClick={postData} className="btn btn-success">Add Vintage</button>
                        </div>
                    </div>
                </div>
            </form>
        </>
    );
}
